Extienda su aplicaci�n para que:
- El usuario pueda jugar contra la maquina (no es necesario hacer nada de inteligencia artificial,
asuma que la m�quina pone fichas de manera aleatoria en el tablero).
- El usuario pueda ajustar el tablero de juego a alg�n tama�o deseado (X filas por Y columnas).
Para desarrollar esta parte recuerde que m�nimo debe tener 4 filas y 4 columnas
- Que haya un modo multijugador (2 o m�s) en el que cada jugador elija con qu� s�mbolo (Una
letra o un n�mero) desea jugar en el tablero
- Los jugadores deben registrarse con su nombre
- Recuerde que se puede ganar bien sea verticalmente, horizontalmente o en diagonal
- La aplicaci�n debe �dirigir� el juego (A quien le toca jugar), avisar cuando hay un ganador y
debe habilitar una opci�n para jugar una nueva partida una vez terminada la actual.
- Extienda su aplicaci�n para que el usuario pueda elegir el n�mero de �fichas� juntas con las
que se gana.
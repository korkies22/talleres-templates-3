package taller.interfaz;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

import taller.mundo.Jugador;
import taller.mundo.LineaCuatro;

public class Interfaz 
{
	/**
	 * Juego actual
	 */
	private LineaCuatro juego;

	/**
	 * Escáner de texto ingresado por el usuario en la consola
	 */
	private Scanner sc;

	/**
	 * Crea una nueva instancia de la clase de interacción del usuario con la consola
	 */
	public Interfaz()
	{
		sc= new Scanner (System.in);
		while (true)
		{
			imprimirMenu();
			try
			{
				int opt=Integer.parseInt(sc.next());
				if(opt==1)
				{
					empezarJuego();
				}
				else if(opt==2)
				{
					empezarJuegoMaquina();
				}
				else if(opt==3)
				{
					System.out.println("¡Vuelva pronto!");
					break;
				}
				else
				{
					System.out.println("Comando inválido");
					continue;
				}
			}
			catch (NumberFormatException e)
			{
				System.out.println("Comando inválido");
				continue;
			}


		}
	}
	/**
	 * Imprime el menú principal
	 */
	public void imprimirMenu()
	{
		System.out.println("------------------------LÍNEA CUATRO------------------------");
		System.out.println("-Menú principal-");
		System.out.println("Ingrese un comando:");
		System.out.println("1. Empezar juego con amigos");
		System.out.println("2. Empezar juego vs máquina");
		System.out.println("3. Salir");		
	}

	/**
	 * Crea un nuevo juego
	 */
	public void empezarJuego()
	{
		System.out.println("ingrese número de filas");
		int filas=sc.nextInt();
		if(filas<4)
		{
			System.out.println("El número de filas debe ser mayor a 4");
			return;
		}
		System.out.println("ingrese número de columnas");
		int columnas=sc.nextInt();
		if(columnas<4)
		{
			System.out.println("El número de filas debe ser mayor a 4");
			return;
		}
		System.out.println("Ingrese número de jugadores");
		int numJugadores= sc.nextInt();
		if(numJugadores<0)
		{
			System.out.println("El número de filas debe ser mayor a 4");
			return;
		}
		ArrayList jugadores= new ArrayList<>();
		for (int i = 0; i < numJugadores; i++)
		{
			int num=i+1;
			System.out.println("Nombre del jugador " + num);
			String nombre= sc.next();
			System.out.println("Símbolo del jugador " + num);
			String simbolo= sc.next();
			Jugador act= new Jugador(nombre, " "+simbolo+ " ");
			jugadores.add(act);
		}
		System.out.println("Ingrese número de fichas para ganar");
		int numGanar= sc.nextInt();
		juego= new LineaCuatro(jugadores, filas, columnas, numGanar);
		juego();
	}

	/**
	 * Modera el juego entre jugadores
	 */
	public void juego()
	{
		while(!juego.fin())
		{
			System.out.println("Turno de "+ juego.darAtacante());
			imprimirTablero();
			System.out.println("escriba el número de columna en que desea dejar caer la ficha");
			int jugada= sc.nextInt();
			if(juego.registrarJugada(jugada))
			{	
				juego.avanzarTurno();
			}
			else
			{
				System.out.println("La jugada no se puede hacer");
			}
		}
		imprimirTablero();

	}
	/**
	 * Empieza el juego contra la máquina
	 */
	public void empezarJuegoMaquina()
	{
		System.out.println("ingrese número de filas");
		int filas=sc.nextInt();
		if(filas<4)
		{
			System.out.println("El número de filas debe ser mayor a 4");
			return;
		}
		System.out.println("ingrese número de columnas");
		int columnas=sc.nextInt();
		if(columnas<4)
		{
			System.out.println("El número de filas debe ser mayor a 4");
			return;
		}
		ArrayList jugadores= new ArrayList<>();
		System.out.println("Nombre del jugador ");
		String nombre= sc.next();
		System.out.println("Símbolo del jugador ");
		String simbolo= sc.next();
		Jugador act= new Jugador(nombre, " "+simbolo+ " ");
		jugadores.add(act);
		System.out.println("Nombre de la máquina");
		nombre= sc.next();
		System.out.println("Símbolo de la máquina");
		simbolo= sc.next();
		Jugador maq= new Jugador(nombre, " "+simbolo+ " ");
		jugadores.add(maq);
		
		System.out.println("Ingrese número de fichas para ganar");
		int numGanar= sc.nextInt();
		juego= new LineaCuatro(jugadores, filas, columnas, numGanar);
		juegoMaquina();
	}
	/**
	 * Modera el juego contra la máquina
	 */
	public void juegoMaquina()
	{
		while(!juego.fin())
		{
			System.out.println("Turno de "+ juego.darAtacante());
			imprimirTablero();
			System.out.println("escriba el número de columna en que desea dejar caer la ficha");
			int jugada= sc.nextInt();
			if(juego.registrarJugada(jugada))
			{	
				juego.avanzarTurno();
			}
			else
			{
				System.out.println("La jugada no se puede hacer");
			}
			if(!juego.fin())
			{
			juego.registrarJugadaAleatoria();
			}
			juego.avanzarTurno();
		}
		imprimirTablero();
	}

	/**
	 * Imprime el estado actual del juego
	 */
	public void imprimirTablero()
	{
		String[][] tablero= juego.darTablero();
		System.out.print("  ");
		for (int i = 0; i < tablero[0].length; i++) 
		{
			System.out.print(i);
			System.out.print("     ");
		}
		System.out.println();
		for (int i = 0; i < tablero.length; i++)
		{
			System.out.print(i);
			for (int j = 0; j < tablero[0].length; j++)
			{

				System.out.print(tablero[i][j]);
				System.out.print("   ");
			}
			System.out.println();
		}
	}
}

package taller.mundo;

import java.util.ArrayList;
import java.util.Random;

public class LineaCuatro 
{
	/**
	 * Matriz que representa el estado actual del juego.
	 */
	private String[][] tablero;

	/**
	 * Lista que contiene a los jugadores
	 */
	private ArrayList<Jugador> jugadores;

	/**
	 * Nombre del jugador en turno
	 */
	private String atacante;

	/**
	 * Determina si el juego se terminó
	 */
	private boolean finJuego;

	/**
	 * El número del jugador en turno
	 */
	private int turno;

	/**
	 * El número de fichas consecutivas para ganar
	 */
	private int numGanar;

	/**
	 * Número de la jugada en que el  juego va
	 */
	private int numJugada;

	/**
	 * Crea una nueva instancia del juego
	 */
	public LineaCuatro(ArrayList <Jugador> pJugadores,int pFil, int pCol, int pNumGanar)
	{
		tablero = new String[pFil][pCol];
		for(int i=0;i<tablero.length;i++)
		{
			for(int j=0;j<tablero[0].length;j++)
			{
				tablero[i][j]="___";
			}

		}
		jugadores = pJugadores;
		finJuego = false;
		turno = 0;
		numJugada=0;
		numGanar=pNumGanar;
		atacante = jugadores.get(turno).darNombre();
	}
	/**
	 * Retorna el tablero del juego
	 * @return tablero
	 */
	public String[][] darTablero()
	{
		return tablero;
	}

	/**
	 * Retorna el jugador en turno
	 * @return atacante jugador en turno
	 */
	public String darAtacante()
	{
		return atacante;
	}


	/**
	 * Determina si el juego termina
	 * @return true si el juego se termino, false de lo contrario
	 */
	public boolean fin()
	{
		return finJuego;
	}

	/**
	 * Retorna el número de fichas consecutivas necesarias para ganar
	 * @return numGanar
	 */
	public int darNumGanar()
	{
		return numGanar;
	}

	public void avanzarTurno() 
	{
		turno++;
		turno=turno%jugadores.size();
		atacante= jugadores.get(turno).darNombre();


	}
	/**
	 * Registra una jugada aleatoria
	 */
	public void registrarJugadaAleatoria()
	{
		ArrayList<Integer> disponibles= new ArrayList<>();
		for (int i = 0; i < tablero[0].length; i++)
		{
			if(tablero[0][i].equals("___"))
			{
				disponibles.add(i);
			}
		}
		Random randomGenerator = new Random();
		int index = randomGenerator.nextInt(disponibles.size());
		int columna = disponibles.get(index);
		registrarJugada(columna);
	}

	/**
	 * Registra una jugada en el tablero
	 * @return true si la jugada se pudo realizar, false de lo contrario
	 */
	public boolean registrarJugada(int col)
	{
		if(col>=0 && col <tablero[0].length && tablero[0][col].equals("___"))
		{
			numJugada++;
			boolean insertar=false;
			for (int i = tablero.length-1; i < tablero.length && !insertar; i--)
			{
				if(tablero[i][col].equals("___"))
				{
					insertar=true;
					Jugador act= jugadores.get(turno);
					tablero[i][col]=act.darSimbolo();
					if(terminar(i,col))
					{
						finJuego=true;
					}
				}
			}
			return true;
		}
		else
		{
			return false;
		}

	}

	/**
	 * Determina si una jugada causa el fin del juego
	 * @return true si la jugada termina el juego false de lo contrario
	 */
	public boolean terminar(int fil,int col)
	{

		String simbolo= jugadores.get(turno).darSimbolo();
		int num=0;
		for (int i = 0; fil+i < tablero.length; i++)
		{
			if(tablero[fil+i][col].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		for (int i = 0; fil-i>=0; i++)
		{
			if(tablero[fil-i][col].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		if(num-1>=numGanar)
		{
			finJuego=true;
			
		}
		num=0;
		for (int i = 0; col+i < tablero[0].length && !finJuego; i++)
		{
			if(tablero[fil][col+i].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		for (int i = 0; col-i>=0 && !finJuego; i++)
		{
			if(tablero[fil][col-i].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		if(num-1>=numGanar)
		{
			finJuego=true;
		}
		num=0;
		for (int i = 0; fil-i>=0 && col-i>=0 && !finJuego; i++)
		{
			if(tablero[fil-i][col-i].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		for (int i = 0; fil+i<tablero.length && col+i<tablero[0].length && !finJuego; i++)
		{
			if(tablero[fil+i][col+i].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		if(num-1>=numGanar)
		{
			finJuego=true;
		}
		num=0;
		for (int i = 0; fil-i>=0 && col+i<tablero[0].length && !finJuego; i++)
		{
			if(tablero[fil-i][col+i].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		for (int i = 0; fil+i<tablero.length && col-i>=0 && !finJuego; i++)
		{
			if(tablero[fil+i][col-i].equals(simbolo))
			{
				num++;
			}
			else
			{
				break;
			}
		}
		if(num-1>=numGanar)
		{
			finJuego=true;
		}
		if(finJuego)
		{
			System.out.println("felicidades, ganó el jugador: " + atacante);
		}
		if(!finJuego && numJugada==tablero.length*tablero[0].length)
		{
			finJuego=true;
			System.out.println("No hay más jugadas por lo que se acaba el juego");
		}

		return finJuego;
	}


}
